//
//  LoadingAnimationView.swift
//  ImageStack
//
//  Created by Kierkegaard on 22/08/19.
//  Copyright © 2019 Moi. All rights reserved.
//

import Foundation
import UIKit

enum GeneralError: Error {
    case withMessage(String)
}

extension UIView {
    func fitLayers() {
        layer.fit(rect: bounds)
    }
}

extension CALayer {
    func fit(rect: CGRect) {
        frame = rect
        if let gradientLayer = self as? CAGradientLayer {
            let oldStartAndEndPoints = AnimatingGradientView.oldStartAndEndPoints(forGradientType: gradientLayer.type, inFrame: frame)
            gradientLayer.startPoint = oldStartAndEndPoints[0]
            gradientLayer.endPoint = oldStartAndEndPoints[1]
        }

        sublayers?.forEach { $0.fit(rect: rect) }
    }
}

class AnimatingGradientView: UIView {
    
    private (set) var isAnimating: Bool = false
    private var animatingGradientLayer: CAGradientLayer?
    var animationDuration: TimeInterval?
    private let defaultColors = [UIColor.black.cgColor, UIColor.white.cgColor]
    var colors: [CGColor]? = [UIColor.black.cgColor, UIColor.white.cgColor] {
        didSet {
            if var colors = colors {
                if colors.count == 0 {
                    colors.append(UIColor.black.cgColor)
                    self.colors = colors
                }
                else if colors.count == 1 {
                    colors.append(UIColor.white.cgColor)
                    self.colors = colors
                }
                else {
                    self.animatingGradientLayer?.colors = colors
                }
            }
            else {
                self.colors = defaultColors
            }
        }
    }
    var gradientType: CAGradientLayerType? {
        didSet {
            self.animatingGradientLayer?.type = gradientType!
        }
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        //Setup animating gradient layer
        self.animatingGradientLayer = CAGradientLayer.init()
        self.layer.backgroundColor = UIColor.red.cgColor
        
        self.layer.addSublayer(self.animatingGradientLayer!)
        self.animatingGradientLayer!.bounds = self.layer.bounds
        
        
        
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.light);
        let blurOverlay = UIVisualEffectView.init(effect: blurEffect)
        self.addSubview(blurOverlay)
        let blurLeft = self.leadingAnchor.constraint(equalTo:self.leadingAnchor)
        let blurRight = self.trailingAnchor.constraint(equalTo:self.trailingAnchor)
        let blurTop = self.topAnchor.constraint(equalTo:self.topAnchor)
        let blurBottom = self.bottomAnchor.constraint(equalTo:self.bottomAnchor)
        NSLayoutConstraint.activate([blurLeft, blurRight, blurTop, blurBottom])
        
        //Defer is required because only then is didSet is called
        defer {
            //Setup defaults for AnimatingGradientLayer
            //Defaults
            //Fixed colors
            self.setRandomizedColors()
            
            self.animationDuration = 5.8
            self.gradientType = CAGradientLayerType.conic
            self.animatingGradientLayer!.colors = self.colors
    
    //        self.colors = [
    //            UIColor.init(red:104/255.0, green:133/255.0, blue:208/255.0, alpha:1).cgColor,
    //            UIColor.init(red:188/255.0, green:141/255.0, blue:61/255.0, alpha:1).cgColor,
    //            UIColor.init(red:183/255.0, green:95/255.0, blue:179/255.0, alpha:1).cgColor,
    //            UIColor.init(red:105/255.0, green:167/255.0, blue:91/255.0, alpha:1).cgColor
    //        ]
        }
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let wasAnimating = self.isAnimating
        if wasAnimating == true {
            self.stopAnimating()
        }
        
        fitLayers()
        
        if wasAnimating == true {
            self.startAnimating()
        }
    }
    
    func startAnimating() {
        self.isAnimating = true
        
        CATransaction.begin()
        //CATransaction.setAnimationDuration(1.6)
        CATransaction.setAnimationDuration(self.animationDuration!)
        CATransaction.setAnimationTimingFunction(CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear))
        
        let startPoint = self.animatingGradientLayer!.startPoint
        let endPoint = self.animatingGradientLayer!.endPoint
        let newStartAndEndPoints = AnimatingGradientView.newStartAndEndPoints(forGradientType: self.animatingGradientLayer!.type, givenOldStartAndEndPoints: [startPoint, endPoint], inFrame:self.bounds)
        let newStartPoint = newStartAndEndPoints[0]
        let newEndPoint = newStartAndEndPoints[1]
        
        let startPointAnimation = CABasicAnimation(keyPath: #keyPath(CAGradientLayer.startPoint))
        startPointAnimation.fromValue = startPoint
        startPointAnimation.toValue = newStartPoint
        startPointAnimation.fillMode = CAMediaTimingFillMode.forwards
        startPointAnimation.isRemovedOnCompletion = false
        startPointAnimation.autoreverses = true
        startPointAnimation.repeatCount = Float.infinity
        
        let endPointAnimation = CABasicAnimation(keyPath: #keyPath(CAGradientLayer.endPoint))
        endPointAnimation.fromValue = endPoint
        endPointAnimation.toValue = newEndPoint
        endPointAnimation.fillMode = CAMediaTimingFillMode.forwards
        endPointAnimation.isRemovedOnCompletion = false
        endPointAnimation.autoreverses = true
        endPointAnimation.repeatCount = Float.infinity
        
        self.animatingGradientLayer!.add(startPointAnimation, forKey: #keyPath(CAGradientLayer.startPoint))
        self.animatingGradientLayer!.add(endPointAnimation, forKey: #keyPath(CAGradientLayer.endPoint))
        
        CATransaction.commit()
    }
    
    func stopAnimating() {
        self.isAnimating = false
        self.animatingGradientLayer!.removeAllAnimations()
    }
    
    class func oldStartAndEndPoints(forGradientType gradientType: CAGradientLayerType, inFrame frame: CGRect) -> [CGPoint] {
        //SCALE X AND Y RADIUS ACCORDING TO ASPECT RATIO OF OVERALL VIEW
        if gradientType == CAGradientLayerType.conic {
            let startPoint = CGPoint(x: 0.5, y:0.5)
            let xDiff = 0.5 as CGFloat
            let yDiff = xDiff * frame.size.width / frame.size.height
            let endPoint = CGPoint(x: startPoint.x + xDiff, y: startPoint.y + yDiff)
            
            return [startPoint, endPoint]
        }
        else if gradientType == CAGradientLayerType.radial {
            let startPoint = CGPoint(x: 0.5, y:0.5)
            let xDiff = 1.6 as CGFloat
            let yDiff = xDiff * frame.size.width / frame.size.height
            let endPoint = CGPoint(x: startPoint.x + xDiff, y: startPoint.y + yDiff)
            
            return [startPoint, endPoint]
        }
        else if gradientType == CAGradientLayerType.axial {
            let startPoint = CGPoint(x: -0.2, y:0.5)
            let endPoint = CGPoint(x: 1.0, y: 0.5)
            
            return [startPoint, endPoint]
        }
        else {
            assert(false, "Invalid GradientLayerType")
        }
    }
    
    class func newStartAndEndPoints(forGradientType gradientType: CAGradientLayerType, givenOldStartAndEndPoints oldStartAndEndPoints: [CGPoint], inFrame frame: CGRect) -> [CGPoint] {
        if gradientType == CAGradientLayerType.conic {
            let oldStartPoint = oldStartAndEndPoints[0]
            let xDiff = -1.0 as CGFloat
            let yDiff = -1.0 as CGFloat
            let newStartPoint = oldStartPoint
            let newEndPoint = CGPoint(x: oldStartPoint.x+xDiff, y: oldStartPoint.y+yDiff)
            
            return [newStartPoint, newEndPoint]
        }
        else if gradientType == CAGradientLayerType.radial {
            let oldStartPoint = oldStartAndEndPoints[0]
            let oldEndPoint = oldStartAndEndPoints[1]
            let scale = 1.6 as CGFloat
            let xDiff = (oldEndPoint.x - oldStartPoint.y) * scale
            let yDiff = (oldEndPoint.y - oldStartPoint.y) * scale
            let newStartPoint = oldStartPoint
            let newEndPoint = CGPoint(x: newStartPoint.x + xDiff, y: newStartPoint.y + yDiff)
            
           
            return [newStartPoint, newEndPoint]
        }
        else if gradientType == CAGradientLayerType.axial {
            let startPoint = CGPoint(x: 0.0, y:0.5)
            let endPoint = CGPoint(x: 1.2, y: 0.5)
           
            return [startPoint, endPoint]
        }
        else {
            assert(false, "Invalid GradientLayerType")
        }
    }
    
    
    func setRandomizedColors() {
//            let baseColors = [UIColor.red.cgColor,
//                            UIColor.yellow.cgColor,
//                            UIColor.blue.cgColor,
//                            UIColor.green.cgColor]
//            let baseColors = [ UIColor(red: 104/255, green: 133/255, blue: 208/255, alpha: 1.0).cgColor,
//                            UIColor(red: 188/255, green: 141/255, blue: 61/255, alpha: 1.0).cgColor,
//                            UIColor(red: 183/255, green: 95/255, blue: 179/255, alpha: 1.0).cgColor,
//                            UIColor(red: 105/255, green: 167/255, blue: 91/255, alpha: 1.0).cgColor ]
//        let baseColors = [  UIColor.init(hue: 0.62, saturation: 0.5, brightness: 0.86, alpha: 1.0).cgColor,
//                            UIColor.init(hue: 0.105, saturation: 0.6755, brightness: 0.7373, alpha: 1.0).cgColor,
//                            UIColor.init(hue: 0.841, saturation: 0.481, brightness: 0.718, alpha: 1.0).cgColor,
//                            UIColor.init(hue: 0.303, saturation: 0.455, brightness: 0.655, alpha: 1.0).cgColor ]
        var baseColors: [CGColor] = []
        for _ in 0...3 {
            let newColor = UIColor.init(hue: CGFloat.random(in: 0.0 ..< 1.0),
                                        saturation: CGFloat.random(in: 0.5 ..< 0.7),
                                        brightness: CGFloat.random(in: 0.7 ..< 0.9),
                                        alpha: 1.0)
            baseColors.append(newColor.cgColor)
        }
        
        self.colors = baseColors
    }
}
