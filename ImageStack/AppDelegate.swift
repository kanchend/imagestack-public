//
//  AppDelegate.swift
//  ImageStack
//
//  Created by Kierkegaard on 20/08/19.
//  Copyright © 2019 Moi. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        self.window = UIWindow(frame: UIScreen.main.bounds)
        
        let rootViewController = SearchController()
        //rootViewController.navigationItem.title = "Splash"
        self.window?.rootViewController = rootViewController
        self.window?.backgroundColor = UIColor.black
        self.window?.makeKeyAndVisible()
        
        //self.testImageSearchApi()
        
        return true
    }
    
    func testImageSearchApi () {
        //Testing BingRequestor
        BingRequestor.shared.searchImages(withQuery: "Wallpapers", completion: { result in
            let rootViewController = self.window!.rootViewController!
            let result: Status = result()
            switch result {
                case let .success(summary, debugMsgs, searchResults, _):
                    let message = "Retrieved \(searchResults.count) search results"
                    let alert = UIAlertController(title: "Success", message: message, preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    rootViewController.present(alert, animated: true, completion: nil)
                    print(summary)
                    print("Printing select data from first search result")
                    print("Thumb-url for first result: ", searchResults[0].thumbnailUrl)
                    
                case let .failure(summary, debugMsgs, _):
                    let alert = UIAlertController(title: "Failure", message: summary, preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    rootViewController.present(alert, animated: true, completion: nil)
                    print(summary)
                    print(debugMsgs)
//                case .failure(let summary, let debugMsgs, .none):
//                    let alert = UIAlertController(title: "Failure", message: summary, preferredStyle: UIAlertController.Style.alert)
//                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
//                    rootViewController.present(alert, animated: true, completion: nil)
//                    print(summary)
//                    print(debugMsgs)
            }
        })
    }

}

