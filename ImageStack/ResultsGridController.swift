//
//  SearchGalleryController.swift
//  ImageStack
//
//  Created by Kierkegaard on 02/09/19.
//  Copyright © 2019 Moi. All rights reserved.
//

import Foundation
import UIKit

class ResultsGridController: UIViewController, ResultsPresenter, UICollectionViewDataSource, UICollectionViewDelegate {
    
    var delegate: ResultsDelegate?
    private var resultsGridView: UICollectionView?
    
    override func viewDidLoad() {
        
        //Configure Collection View and Layout for
        //Search Grid
        
        //Configuring grid layout
        let searchFlowLayout = UICollectionViewFlowLayout()
        searchFlowLayout.scrollDirection = UICollectionView.ScrollDirection.vertical
        searchFlowLayout.minimumLineSpacing = 0.0
        searchFlowLayout.minimumInteritemSpacing = 0.0
        searchFlowLayout.sectionInset = UIEdgeInsets.zero
        
        let widthSizeClass = UIScreen.main.traitCollection.horizontalSizeClass
        let heightSizeClass = UIScreen.main.traitCollection.verticalSizeClass
        var deviceSizeClass = UIUserInterfaceSizeClass.regular
        var minCellSize = 160.0 as CGFloat
        if widthSizeClass == .compact || heightSizeClass == .compact {
            deviceSizeClass = UIUserInterfaceSizeClass.compact
            minCellSize = 120.0
        }
        let screenWidth = min(UIScreen.main.bounds.size.width, UIScreen.main.bounds.size.height)
        let numColumns = floor(screenWidth / minCellSize)
        let actualCellSize = Double(screenWidth) / Double(numColumns)
        
        searchFlowLayout.itemSize = CGSize(width: CGFloat(actualCellSize), height: CGFloat(actualCellSize))
        
        
        //Initialise collection view with configured layout
        let resultsGridView = UICollectionView.init(frame: self.view.bounds, collectionViewLayout: searchFlowLayout)
        self.resultsGridView = resultsGridView
        resultsGridView.dataSource = self
        resultsGridView.delegate = self
        resultsGridView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "ResultsGridCell")
        resultsGridView.backgroundColor = UIColor.clear
        resultsGridView.clipsToBounds = false
        self.view.addSubview(resultsGridView)
        
        resultsGridView.translatesAutoresizingMaskIntoConstraints = false
        let rgLeft = resultsGridView.leadingAnchor.constraint(equalTo:self.view.leadingAnchor)
        let rgRight = resultsGridView.trailingAnchor.constraint(equalTo:self.view.trailingAnchor)
        let rgTop = resultsGridView.topAnchor.constraint(equalTo:self.view.topAnchor)
        let rgBottom = resultsGridView.bottomAnchor.constraint(equalTo:self.view.bottomAnchor)
        NSLayoutConstraint.activate([rgLeft, rgRight, rgTop, rgBottom])
    }
    
    //Mark -- ResultsPresenter protocol implementation
    func reloadResults() {
        self.resultsGridView?.reloadData()
    }
    
    func insertResults(inRange rangeToInsert: CountableClosedRange<Int>) {
        print("Request insertion in range", rangeToInsert)
        let indexPathsToInsert = rangeToInsert.map { IndexPath(row: $0, section: 0)}
        print("Calculated index-paths", indexPathsToInsert)
        self.resultsGridView?.insertItems(at: indexPathsToInsert)
        
//        var indexPathsToAdd: [IndexPath] = []
//        for i in 0...(addedResultCount-1) {
//            let newIndexPath = IndexPath(row: initialResultCount + i, section: 0)
//            indexPathsToAdd.append(newIndexPath)
//        }
    }
    
    func focusOnResult(atIndex index: Int, animated: Bool = false) {
        //Scroll to page
        if let resultCount = self.delegate?.resultCount {
            print("Attempting to direct user focus to result at index:", index)
            assert(index >= 0 && index < resultCount, "Invalid index. Requested \(index), expected in range 0...\(resultCount)")
            let indexPathToFocus = IndexPath(row: index, section: 0)
            self.resultsGridView?.scrollToItem(at: indexPathToFocus, at: .centeredVertically, animated: animated)
        }
        else {
            print("Nothing to focus. Search results don't exist anymore")
        }
    }
    
    
    
    //Mark -- UICollectionViewDataSource protocol methods
    func numberOfSections(in: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection: Int) -> Int {
        //Here, delegate can be optional
        if let resultsCount = self.delegate?.resultCount {
            print("Returning search result-count: " + String(resultsCount))
            return resultsCount
        }
        else {
            print("No delegate exists. ResultCount = 0")
            return 0
        }
    }

    // The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let imageCell: UICollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ResultsGridCell", for: indexPath)
        var searchThumbnailView: UIImageView? = imageCell.contentView.viewWithTag(155) as? UIImageView
        if searchThumbnailView != nil {
            //do nothing
            //other than set the image-cell to the size configured in UICollectionViewLayout
            //Might also be able to do this with Auto-Layout but that maybe more expensive?
//            let cellSize = (self.imageResultsView!.collectionViewLayout as! UICollectionViewFlowLayout).itemSize
//            searchThumbnailView!.frame = CGRect(x:0, y:0, width:cellSize.width, height:cellSize.height)
        }
        else {
            let cellSize = (self.resultsGridView!.collectionViewLayout as! UICollectionViewFlowLayout).itemSize
            searchThumbnailView = UIImageView.init(frame: CGRect(x:0, y:0, width:cellSize.width, height:cellSize.height))
            searchThumbnailView!.tag = 155
            searchThumbnailView!.contentMode = UIView.ContentMode.scaleAspectFill
            searchThumbnailView!.clipsToBounds = true
            imageCell.contentView.addSubview(searchThumbnailView!)
            
            searchThumbnailView!.translatesAutoresizingMaskIntoConstraints = false
            let thumbLeft = searchThumbnailView!.leadingAnchor.constraint(equalTo:imageCell.contentView.leadingAnchor)
            let thumbRight = searchThumbnailView!.trailingAnchor.constraint(equalTo:imageCell.contentView.trailingAnchor)
            let thumbTop = searchThumbnailView!.topAnchor.constraint(equalTo:imageCell.contentView.topAnchor)
            let thumbBottom = searchThumbnailView!.bottomAnchor.constraint(equalTo:imageCell.contentView.bottomAnchor)
            NSLayoutConstraint.activate([thumbLeft, thumbRight, thumbTop, thumbBottom])
        }
        
        print("Configuring cell #" + String(indexPath.row))
        
        let previousIndex = imageCell.contentView.tag - 1
        if previousIndex >= 0{
            //Previous-index would be out-of-bounds if
            //a previous search was cleared and a new
            //search was started. The previous search may
            //have loaded 40-50 rows, whereas the newer
            //search may have just started with 15,
            //thus leading to overflow when applying previousIndex
            //to self.searchResults (for the current query)
            if previousIndex < self.delegate?.resultCount ?? 0 {
                let previousResultItem = self.delegate!.resultItem(atIndex: previousIndex)
                //Cancel previous Thumbnail request
                ImageCache.shared.cancelGetImage(forURL: previousResultItem.thumbnailUrl)
            }
            //Reset image if any
            searchThumbnailView!.image = nil
        }
        
        imageCell.contentView.tag = indexPath.row + 1
        if let searchResultItem = self.delegate?.resultItem(atIndex: indexPath.row) {
            searchThumbnailView!.backgroundColor = searchResultItem.accentColor
            let searchThumburl = searchResultItem.thumbnailUrl
            ImageCache.shared.getImage(forURL: searchThumburl, sizeClass: ImageCache.SizeClass.Small,
            onSuccess: { [weak searchThumbnailView] (image: UIImage) -> () in
                
                print("Received image from cache for url", searchThumburl)
                print("Waiting to load image")

                if let searchThumbnailView = searchThumbnailView {
                    UIView.transition(with: searchThumbnailView, duration: 0.3,
                                      options: UIView.AnimationOptions.transitionCrossDissolve,
                                      animations: {
                                        searchThumbnailView.image = image
                                        },
                                      completion: nil)
                }
                else {
                    print("Thumbnail view has been deallocated before closure of image-cache request called. Handled safely")
                }
            },
            onFailure: { (error: Error) -> () in
                //On failure. Log the error and wait for the next refresh
                print("Failed to load search result thumbnail with details: ", searchResultItem)
                print("With error: ", error)
            })
            
            //Now, if we have reached the second-last row of image results,
            //initiate a request for more image results
            let cellWidth = searchThumbnailView!.bounds.size.width
            let numColumns = Int(floor(UIScreen.main.bounds.size.width / cellWidth))
            //Delegate MUST ignore repeated requests
            if indexPath.row >= self.delegate!.resultCount - numColumns * 2 {
                self.delegate!.requestNextPageOfResults(fromIndex: self.delegate!.resultCount)
            }
        }
        
        return imageCell
    }
    
    
    //Mark -- UICollectionViewDelegate methods
//    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
//        return true
//    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Did call selection in Grid view at indexPath", indexPath)
        self.delegate?.presenter(self, didFocusOnResultAtIndex: indexPath.row)
    }

    
    //Adjust image cell size on rotation
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        //super.traitCollectionDidChange(previousTraitCollection)
        
        var minCellSize = 160.0 as CGFloat
        let widthSizeClass = self.traitCollection.horizontalSizeClass
        if widthSizeClass == .compact {
            minCellSize = 120.0
        }
        
        let screenWidth = self.view.bounds.size.width
        let numColumns = floor(screenWidth / minCellSize)
        let actualCellSize = Double(screenWidth) / Double(numColumns)
        
        let searchFlowLayout = self.resultsGridView!.collectionViewLayout as! UICollectionViewFlowLayout
        searchFlowLayout.itemSize = CGSize(width: CGFloat(actualCellSize), height: CGFloat(actualCellSize))
    }
}
