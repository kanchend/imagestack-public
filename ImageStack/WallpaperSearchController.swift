//
//  WallpaperController.swift
//  ImageStack
//
//  Created by Kierkegaard on 29/08/19.
//  Copyright © 2019 Moi. All rights reserved.
//

import Foundation
import UIKit

class WallpaperSearchController: UIViewController {
    
    private var wallpaperSearchResults: [BingImageResultItem]?
    private var wallpaperIndex: Int = 0
    private var wallpaperView: AnimatingRectImageView?
    
    override func loadView() {
        
        let wallpaperView = AnimatingRectImageView.init(frame: CGRect.zero)
        wallpaperView.backgroundColor = UIColor.red
        self.view = wallpaperView
//        let wpLeft = wpView.leadingAnchor.constraint(equalTo:self.view.leadingAnchor)
//        let wpRight = wpView.trailingAnchor.constraint(equalTo:self.view.trailingAnchor)
//        let wpTop = wpView.topAnchor.constraint(equalTo:self.view.topAnchor)
//        let wpBottom = wpView.bottomAnchor.constraint(equalTo:self.view.bottomAnchor)
//        NSLayoutConstraint.activate([wpLeft, wpRight, wpTop, wpBottom])
    }
    
    override func viewDidLoad() {
        
    }
    
    func startLoadingWallpapers() {
        self.searchForWallpapers()
    }
    
    func searchForWallpapers () {
        //Testing BingRequestor
        BingRequestor.shared.searchImages(withQuery: "Wallpapers", completion: { result in
            let result: Status = result()
            switch result {
                case let .success(summary, debugMsgs, searchResults, _):
//                    //Simple test for API response
//                    let message = "Retrieved \(searchResults.count) search results"
//                    let alert = UIAlertController(title: "Success", message: message, preferredStyle: UIAlertController.Style.alert)
//                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
//                    self.present(alert, animated: true, completion: nil)
                    //Simple debug logs
                    print(summary)
                    print("Printing select data from first search result")
                    print("Thumb-url for first result: ", searchResults[0].thumbnailUrl)
//                    //Advanced debug logs
//                    for (type, message) in debugMsgs {
//                        print(type, ": ", message)
//                    }
                    self.wallpaperSearchResults = searchResults
                    self.loadNextWallpaper()
                    
                case let .failure(summary, debugMsgs, _):
//                    let alert = UIAlertController(title: "Failure", message: summary, preferredStyle: UIAlertController.Style.alert)
//                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
//                    self.present(alert, animated: true, completion: nil)
                    print("Unable to load wallpaper images. Retrying in 60 seconds")
                    print(summary)
                    print(debugMsgs)
//                case .failure(let summary, let debugMsgs, .none):
//                    let alert = UIAlertController(title: "Failure", message: summary, preferredStyle: UIAlertController.Style.alert)
//                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
//                    rootViewController.present(alert, animated: true, completion: nil)
//                    print(summary)
//                    print(debugMsgs)
            }
        },

      count: 10,
      nextOffset: 0)
    }
    
    func loadNextWallpaper() {
        if self.wallpaperSearchResults != nil {
            if self.wallpaperIndex >= self.wallpaperSearchResults!.count-1 {
                self.wallpaperIndex = 0
            }
            
            let wallpaperUrl = self.wallpaperSearchResults![self.wallpaperIndex].fullImageUrl
            ImageCache.shared.getImage(forURL: wallpaperUrl, sizeClass: ImageCache.SizeClass.Large,
            onSuccess: { (image: UIImage) -> () in
                
                let wallpaperView = self.view as! AnimatingRectImageView
                UIView.transition(with: wallpaperView, duration: 0.5,
                                  options: UIView.AnimationOptions.transitionCrossDissolve,
                                  animations: {
                                    wallpaperView.image = image
                                    },
                                  completion: nil)
                self.wallpaperIndex+=1
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 5) {
                    self.loadNextWallpaper()
                }
            },
            onFailure: { (error: Error) -> () in
                //On failure. Log the error and simply try the next wallpaper
                print("Failed to load wallpaper with details: ", self.wallpaperSearchResults![self.wallpaperIndex])
                print("With error: ", error)
                self.wallpaperIndex+=1
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 5) {
                    self.loadNextWallpaper()
                }
            })
        }
    }
}
