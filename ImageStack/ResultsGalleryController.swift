//
//  ResultsGalleryController.swift
//  ImageStack
//
//  Created by Kierkegaard on 02/09/19.
//  Copyright © 2019 Moi. All rights reserved.
//

import Foundation
import UIKit

let VIEWTAGPREFIX = 999000

class ResultsGalleryController: UIViewController, ResultsPresenter,
                                UICollectionViewDataSource, UICollectionViewDelegate,
                                UIScrollViewDelegate {
    
    var delegate: ResultsDelegate?
    private var resultsGalleryView: UICollectionView?
    private var transientBGConstraints: [Int: [NSLayoutConstraint]] = [:]
    private var topBar: UIToolbar?
    
    override func viewDidLoad() {
        
        //Configure Collection View and Layout for
        //Search Gallery
        
        //Configuring gallery layout
        let searchFlowLayout = UICollectionViewFlowLayout()
        searchFlowLayout.scrollDirection = UICollectionView.ScrollDirection.horizontal
        searchFlowLayout.minimumLineSpacing = 0.0
        searchFlowLayout.minimumInteritemSpacing = 0.0
        searchFlowLayout.sectionInset = UIEdgeInsets.zero
        searchFlowLayout.itemSize = CGSize(width: self.view.bounds.size.width,
                                           height: self.view.bounds.size.height)
        
        
        //Initialise collection view with configured layout
        let resultsGalleryView = UICollectionView.init(frame: self.view.bounds, collectionViewLayout: searchFlowLayout)
        self.resultsGalleryView = resultsGalleryView
        resultsGalleryView.dataSource = self
        resultsGalleryView.delegate = self
        resultsGalleryView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "ResultsGalleryCell")
        resultsGalleryView.backgroundColor = UIColor.black
        resultsGalleryView.isPagingEnabled = true
        resultsGalleryView.isScrollEnabled = true
        resultsGalleryView.clipsToBounds = true
        self.view.addSubview(resultsGalleryView)
        
        resultsGalleryView.translatesAutoresizingMaskIntoConstraints = false
        let rgLeft = resultsGalleryView.leadingAnchor.constraint(equalTo:self.view.leadingAnchor)
        let rgRight = resultsGalleryView.trailingAnchor.constraint(equalTo:self.view.trailingAnchor)
        let rgTop = resultsGalleryView.topAnchor.constraint(equalTo:self.view.topAnchor)
        let rgBottom = resultsGalleryView.bottomAnchor.constraint(equalTo:self.view.bottomAnchor)
        NSLayoutConstraint.activate([rgLeft, rgRight, rgTop, rgBottom])
        
        
        //Add dismiss gesture
        let swipeDownGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(didSwipeDownToDismiss))
        swipeDownGestureRecognizer.direction = .down
        self.view.addGestureRecognizer(swipeDownGestureRecognizer)
        
        let topBar = UIToolbar.init()
        self.topBar = topBar
        topBar.alpha = 1.0
        topBar.barStyle = UIBarStyle.blackTranslucent
        //topBar.barPosition = UIBarPosition.UIBarPositionTopAtt
        self.view.addSubview(topBar)
        
        topBar.translatesAutoresizingMaskIntoConstraints = false
        let tpLeft = topBar.leadingAnchor.constraint(equalTo:self.view.leadingAnchor)
        let tpRight = topBar.trailingAnchor.constraint(equalTo:self.view.trailingAnchor)
        let tpTop = topBar.topAnchor.constraint(equalTo:self.view.topAnchor)
        let tpHeight = topBar.heightAnchor.constraint(equalToConstant: 80.0)
        NSLayoutConstraint.activate([tpLeft, tpRight, tpTop, tpHeight])
        
        //Add a Done button to the top-bar
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(didTapDone))
        topBar.items = [flexSpace, doneButton]
        
        //Hide top-bar few seconds after appearance
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2) { [weak self] in
            self?.showHideTopbar(hide: true)
        }
    }
    
    //Functions to show/hide top-bar
    func showHideTopbar(hide: Bool) {
        if let topBar = self.topBar {
            topBar.isUserInteractionEnabled = !hide
            UIView.transition(with: topBar, duration: 0.25,
            options: UIView.AnimationOptions.transitionCrossDissolve,
            animations: {
                topBar.alpha = hide ? 0.0 : 1.0
              },
            completion: nil)
        }
    }
    
    func isTopbarHidden() -> Bool {
        return self.topBar?.isUserInteractionEnabled ?? false
    }
    
    
    //Mark -- ResultsPresenter protocol implementation
    func reloadResults() {
        self.resultsGalleryView?.reloadData()
    }
    
    func insertResults(inRange rangeToInsert: CountableClosedRange<Int>) {
        print("Request insertion in range", rangeToInsert)
        let indexPathsToInsert = rangeToInsert.map { IndexPath(row: $0, section: 0)}
        print("Calculated index-paths", indexPathsToInsert)
        self.resultsGalleryView?.insertItems(at: indexPathsToInsert)
        
//        var indexPathsToAdd: [IndexPath] = []
//        for i in 0...(addedResultCount-1) {
//            let newIndexPath = IndexPath(row: initialResultCount + i, section: 0)
//            indexPathsToAdd.append(newIndexPath)
//        }
    }
    
    func focusOnResult(atIndex index: Int, animated: Bool = false) {
        //Scroll to page
        //Sneakily initialize view if "focus" is called before
        //views have even been set-up
        if let resultCount = self.delegate?.resultCount, let _ = self.view {
            assert(index >= 0 && index < resultCount, "Invalid index. Requested \(index), expected in range 0...\(resultCount)")
            let indexPathToFocus = IndexPath(row: index, section: 0)
            if let galleryView = self.resultsGalleryView {
                print("Setting focus on gallery view, at index", index)
                galleryView.scrollToItem(at: indexPathToFocus, at: .left, animated: animated)
                self.view.layoutIfNeeded()
            }
            else {
                print("Gallery view has __not__ been initialized")
            }
        }
        else {
            print("Nothing to focus. Search results don't exist anymore")
        }
    }
    
    
    
    //Mark -- UICollectionViewDataSource protocol methods
    func numberOfSections(in: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection: Int) -> Int {
        //Here, delegate can be optional
        if let resultsCount = self.delegate?.resultCount {
            print("Returning search result-count: " + String(resultsCount))
            return resultsCount
        }
        else {
            print("No delegate exists. ResultCount = 0")
            return 0
        }
    }

    // The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let imageCell: UICollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ResultsGalleryCell", for: indexPath)
        var imageZoomView: UIScrollView? = imageCell.contentView.viewWithTag(VIEWTAGPREFIX + 153) as? UIScrollView
        if imageZoomView != nil {
            //do nothing
        }
        else {
            let cellSize = (self.resultsGalleryView!.collectionViewLayout as! UICollectionViewFlowLayout).itemSize
            imageZoomView = UIScrollView.init(frame: CGRect(x:0, y:0, width: cellSize.width, height: cellSize.height))
            imageZoomView!.tag = VIEWTAGPREFIX + 153
            imageZoomView!.backgroundColor = UIColor.black
            imageZoomView!.minimumZoomScale = 1.0
            imageZoomView!.maximumZoomScale = 1.0
            imageZoomView!.clipsToBounds = true
            imageZoomView!.delegate = self
            imageZoomView!.showsHorizontalScrollIndicator = false
            imageZoomView!.showsVerticalScrollIndicator = false
            imageCell.contentView.addSubview(imageZoomView!)
            
            //Add double-tap recognizer to zoom view
            let doubleTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.didDoubleTapResultCell(_:)))
            doubleTapRecognizer.numberOfTapsRequired = 2
            imageZoomView!.addGestureRecognizer(doubleTapRecognizer)
            
            //Add single-tap recognizer to zoom view itself,
            //since it requires the double-tap to fail,
            //and the appropriate home for the double-tap is
            //indeed on the zoom view - even or ESPECIALLY
            //if this were to be separated into a UICollectionView
            //subclass or a UIImageView+UIScrollView subclass.
            let singleTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.didSingleTapResultCell(_:)))
            singleTapRecognizer.numberOfTapsRequired = 1
            singleTapRecognizer.require(toFail: doubleTapRecognizer)
            imageZoomView!.addGestureRecognizer(singleTapRecognizer)
            
            //Scale scroll-view automatically to contaning cell's size.
            imageZoomView!.translatesAutoresizingMaskIntoConstraints = false
            let izLeft = imageZoomView!.widthAnchor.constraint(equalTo:imageCell.contentView.widthAnchor)
            let izRight = imageZoomView!.heightAnchor.constraint(equalTo:imageCell.contentView.heightAnchor)
            let izTop = imageZoomView!.centerXAnchor.constraint(equalTo:imageCell.contentView.centerXAnchor)
            let izBottom = imageZoomView!.centerYAnchor.constraint(equalTo:imageCell.contentView.centerYAnchor)
            NSLayoutConstraint.activate([izLeft, izRight, izTop, izBottom])
            
            //Setup image view
            let searchThumbnailView = UIImageView.init(frame: CGRect(x:0, y:0, width:cellSize.width, height:cellSize.height))
            searchThumbnailView.tag = VIEWTAGPREFIX + 155
            searchThumbnailView.backgroundColor = UIColor.clear
            searchThumbnailView.contentMode = UIView.ContentMode.scaleAspectFit
            searchThumbnailView.clipsToBounds = true
            imageZoomView!.addSubview(searchThumbnailView)
            
            searchThumbnailView.translatesAutoresizingMaskIntoConstraints = false
            let thumbLeft = searchThumbnailView.widthAnchor.constraint(equalTo:imageZoomView!.widthAnchor)
            let thumbRight = searchThumbnailView.heightAnchor.constraint(equalTo:imageZoomView!.heightAnchor)
            let thumbTop = searchThumbnailView.centerXAnchor.constraint(equalTo:imageZoomView!.centerXAnchor)
            let thumbBottom = searchThumbnailView.centerYAnchor.constraint(equalTo:imageZoomView!.centerYAnchor)
            NSLayoutConstraint.activate([thumbLeft, thumbRight, thumbTop, thumbBottom])
            
            
            let loadingBG = UIView.init(frame: CGRect(x:0, y:0, width:cellSize.width, height:cellSize.height))
            loadingBG.tag = VIEWTAGPREFIX + 154
            loadingBG.backgroundColor = UIColor.orange
            loadingBG.translatesAutoresizingMaskIntoConstraints = false
            imageZoomView!.insertSubview(loadingBG, belowSubview: searchThumbnailView)
            let lbXCenter = loadingBG.centerXAnchor.constraint(equalTo:imageZoomView!.centerXAnchor)
            let lbYCenter = loadingBG.centerYAnchor.constraint(equalTo:imageZoomView!.centerYAnchor)
            NSLayoutConstraint.activate([lbXCenter, lbYCenter])
            
            let loadingBar = FTLinearActivityIndicator()
            loadingBar.tag = VIEWTAGPREFIX + 156
            loadingBar.tintColor = UIColor.white
            loadingBar.isHidden = false
            imageCell.contentView.addSubview(loadingBar)
            loadingBar.translatesAutoresizingMaskIntoConstraints = false
            let lb_XCenter = loadingBar.centerXAnchor.constraint(equalTo: imageCell.contentView.centerXAnchor)
            let lbWidth = loadingBar.widthAnchor.constraint(equalToConstant:100)
            let lbHeight = loadingBar.heightAnchor.constraint(equalToConstant: 10)
            let lbBottom = loadingBar.bottomAnchor.constraint(equalTo: imageCell.contentView.bottomAnchor, constant: -40.0)
            NSLayoutConstraint.activate([lb_XCenter, lbWidth, lbHeight, lbBottom])
        }
        
        let loadingBG: UIView = imageZoomView!.viewWithTag(VIEWTAGPREFIX + 154)!
        let loadingBar: FTLinearActivityIndicator = imageCell.contentView.viewWithTag(VIEWTAGPREFIX + 156) as! FTLinearActivityIndicator
        let searchThumbnailView: UIImageView = imageZoomView!.viewWithTag(VIEWTAGPREFIX + 155) as! UIImageView
        print("Configuring cell #" + String(indexPath.row))
        
        let previousIndex = imageCell.contentView.tag - 1 - VIEWTAGPREFIX
        if previousIndex >= 0{
            //Previous-index would be out-of-bounds if
            //a previous search was cleared and a new
            //search was started. The previous search may
            //have loaded 40-50 rows, whereas the newer
            //search may have just started with 15,
            //thus leading to overflow when applying previousIndex
            //to self.searchResults (for the current query)
            if previousIndex < self.delegate?.resultCount ?? 0 {
                if let previousBGConstraints = self.transientBGConstraints[previousIndex] {
                    NSLayoutConstraint.deactivate(previousBGConstraints)
                    self.transientBGConstraints.removeValue(forKey: previousIndex)
                }
                let previousResultItem = self.delegate!.resultItem(atIndex: previousIndex)
                //Cancel previous Thumbnail request
                ImageCache.shared.cancelGetImage(forURL: previousResultItem.thumbnailUrl)
            }
            //Reset image if any
            searchThumbnailView.image = nil
        }
        
        imageCell.contentView.tag = VIEWTAGPREFIX + indexPath.row + 1
        if let searchResultItem = self.delegate?.resultItem(atIndex: indexPath.row) {
            //Prepare image-view with correct scale, accent color etc to
            //receive thumb or final image
            loadingBG.backgroundColor = searchResultItem.accentColor
            let aspectRatio = searchResultItem.imageSize.height / searchResultItem.imageSize.width
            var bgConstraints: [NSLayoutConstraint] = []
            
            let screenHeight = self.view.bounds.size.height
            let screenWidth = self.view.bounds.size.width
            let screenAspectRatio = screenHeight / screenWidth
            var maximumZoomScale: CGFloat = 1.0
            //Image will be constrained by height as long as the
            //aspect ratio of the IMAGE is more than the aspect
            //ratio of the DISPLAY
            if aspectRatio > screenAspectRatio {
                let bgHeightConstraint = loadingBG.heightAnchor.constraint(equalTo: imageCell.contentView.heightAnchor)
                let bgWidthConstraint = loadingBG.widthAnchor.constraint(equalTo: imageCell.contentView.heightAnchor, multiplier: 1.0/aspectRatio)
                bgConstraints.append(bgHeightConstraint)
                bgConstraints.append(bgWidthConstraint)
                
                maximumZoomScale = searchResultItem.imageSize.height / imageCell.contentView.frame.size.height
            }
            else {
                let bgWidthConstraint = loadingBG.widthAnchor.constraint(equalTo: imageCell.contentView.widthAnchor)
                let bgHeightConstraint = loadingBG.heightAnchor.constraint(equalTo: imageCell.contentView.widthAnchor, multiplier: aspectRatio)
                bgConstraints.append(bgWidthConstraint)
                bgConstraints.append(bgHeightConstraint)
                
                maximumZoomScale = searchResultItem.imageSize.width / imageCell.contentView.frame.size.width
            }
            self.transientBGConstraints[indexPath.row] = bgConstraints
            NSLayoutConstraint.activate(bgConstraints)
//            imageCell.contentView.layoutIfNeeded()
            
            //Reset scroll-view for showing new image
            imageZoomView!.contentSize = searchResultItem.imageSize //searchThumbnailView.bounds.size
            imageZoomView!.zoomScale = imageZoomView!.minimumZoomScale
            imageZoomView!.contentOffset = CGPoint(x:0, y:0)
            imageZoomView!.maximumZoomScale = maximumZoomScale
            
            loadingBar.startAnimating()
            let searchThumburl = searchResultItem.thumbnailUrl
            ImageCache.shared.getImage(forURL: searchThumburl, sizeClass: ImageCache.SizeClass.Small,
            onSuccess: { [weak searchThumbnailView, weak loadingBG] (image: UIImage) -> () in
                
                print("Received thumbnail from cache for url", searchThumburl)
                print("Waiting to load image")

                if let searchThumbnailView = searchThumbnailView {
                    print("Loading image")
                    UIView.transition(with: searchThumbnailView, duration: 0.3,
                                      options: UIView.AnimationOptions.transitionCrossDissolve,
                                      animations: {
                                        loadingBG?.backgroundColor = UIColor.black
                                        searchThumbnailView.image = image
                                        },
                                      completion: nil)
                }
                else {
                    print("Thumbnail view has been deallocated before closure of image-cache request called. Handled safely")
                }
            },
            onFailure: { (error: Error) -> () in
                //On failure. Log the error and wait for the next refresh
                print("Failed to load search result thumbnail with details: ", searchResultItem)
                print("With error: ", error)
            })
            
            //ONLY in case of Full-Screen gallery view,
            //inform the delegate that "didFocus"
            self.delegate?.presenter(self, didFocusOnResultAtIndex: indexPath.row)
            
            //If we are reaching the last image result, we have to initiate a request for more
            //Note: Repeated requests MUST be ignored by delegate
            if indexPath.row >= self.delegate!.resultCount-5 {
                self.delegate!.requestNextPageOfResults(fromIndex: self.delegate!.resultCount)
            }
        }
        
        return imageCell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay imageCell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if let imageZoomView: UIScrollView = imageCell.contentView.viewWithTag(VIEWTAGPREFIX + 153) as? UIScrollView {
            let loadingBar: FTLinearActivityIndicator = imageCell.contentView.viewWithTag(VIEWTAGPREFIX + 156) as! FTLinearActivityIndicator
            let searchThumbnailView: UIImageView = imageZoomView.viewWithTag(VIEWTAGPREFIX + 155) as! UIImageView
            print("Called will display on cell #" + String(indexPath.row))
            //First, stop any previously started animation
            loadingBar.stopAnimating()
            
            if let searchResultItem = self.delegate?.resultItem(atIndex: indexPath.row) {
                let fullImageUrl = searchResultItem.fullImageUrl
                
                //Load full-size image ONLY if user is on this screen for more
                //than half a second
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0) { [weak searchThumbnailView, weak loadingBar] in
                    
                    //During this dispatch wait-period of 1.0 seconds, it is possible that
                    //user has scrolled away and cell has been reused for a different image.
                    //Perform sanity check on whether the cell is STILL holding the same result item,
                    //or has been reused for a different index.
                    let currentResultIndex = imageCell.contentView.tag - 1 - VIEWTAGPREFIX
                    if indexPath.row == currentResultIndex {
                        //Sanity check passed
                        loadingBar?.startAnimating()
                        
                        //Now place request for full-image url
                        ImageCache.shared.getImage(forURL: fullImageUrl, sizeClass: ImageCache.SizeClass.Large,
                        onSuccess: { [weak searchThumbnailView, weak loadingBar] (image: UIImage) -> () in
                            
                            print("Received full-size image from cache for url", fullImageUrl)
                            print("For request sent from Original cell #", indexPath.row )
                            //Since loading full-size images takes time, it is possible
                            //user has scrolled away and cell has been reused for a different image.
                            //Perform sanity check on whether the cell is STILL holding the same result item,
                            //or has been reused for a different index.
                            let currentResultIndex = imageCell.contentView.tag - 1 - VIEWTAGPREFIX
                            print("Current cell #", currentResultIndex)
                            if currentResultIndex >= 0 && currentResultIndex < self.delegate?.resultCount ?? 0{
                                if let currentResultItem = self.delegate?.resultItem(atIndex: currentResultIndex) {
                                    if currentResultItem.fullImageUrl == fullImageUrl {
                                        print("Sanity check passed. Full-size image-url matches result being displayed")
                                        print("Waiting to load image")
                                        
                                        if let searchThumbnailView = searchThumbnailView {
                                            print("Loaded image")
                                            UIView.transition(with: searchThumbnailView, duration: 0.3,
                                                              options: UIView.AnimationOptions.transitionCrossDissolve,
                                                              animations: {
                                                                searchThumbnailView.image = image
                                                                loadingBar?.stopAnimating()
                                                                },
                                                              completion: nil)
                                        }
                                        else {
                                            print("Image view dealloced. Aborting")
                                        }
                                    }
                                    else {
                                        print("Sanity check failed. Full-size image-url does NOT match result being displayed. Aborting")
                                    }
                                }
                                else {
                                    print("Sanity check failed. Invalid cell-index. Aborting")
                                }
                            }
                            else {
                                print("Sanity check failed. Invalid cell-index. Aborting")
                            }
                        },
                        onFailure: { (error: Error) -> () in
                            //On failure. Log the error and wait for the next refresh
                            print("Failed to load search result full-image with details: ", searchResultItem)
                            print("With error: ", error)
                            loadingBar?.stopAnimating()
                        })
                    }
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying imageCell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        print("Called did-end-Displaying on cell #" + String(indexPath.row))
        
        if let loadingBar = imageCell.contentView.viewWithTag(VIEWTAGPREFIX + 156) as? FTLinearActivityIndicator {
            //Stop image-loading animation
            loadingBar.stopAnimating()
        }
        
        if let searchResultItem = self.delegate?.resultItem(atIndex: indexPath.row) {
            print("Cancelled full-size image-load for url", searchResultItem.fullImageUrl)
            //Cancel previous full-image request if any
            ImageCache.shared.cancelGetImage(forURL: searchResultItem.fullImageUrl)
        }
    }
    
    //Mark -- UICollectionViewDelegate methods
//    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//
//        let searchFlowLayout = self.resultsGalleryView!.collectionViewLayout as! UICollectionViewFlowLayout
//
//        let x = scrollView.contentOffset.x
//        let w = searchFlowLayout.itemSize.width
//        let currentPage = Int(ceil(x/w))
//
//        self.delegate?.presenter(self, didFocusOnResultAtIndex: currentPage-1)
//    }
    
//    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
//        return true
//    }
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        print("Did call selection in Gallery view at indexPath", indexPath)
//        self.delegate?.presenter(self, didFocusOnResultAtIndex: indexPath.row)
//    }

    @objc func didSwipeDownToDismiss() {
        print("Did swipe down to Dismiss")
        self.delegate?.presenterDidRequestToDismiss(self, animated: true)
    }
    
    @objc func didTapDone() {
        print("Did tap Done button")
        self.delegate?.presenterDidRequestToDismiss(self, animated: true)
    }
    
    @objc func didSingleTapResultCell(_ fromRecognizer: UIGestureRecognizer) {
        if self.topBar?.isUserInteractionEnabled == true {
            self.showHideTopbar(hide: true)
        }
        else {
            self.showHideTopbar(hide: false)
        }
    }
    
    @objc func didDoubleTapResultCell(_ doubleTapRecognizer: UITapGestureRecognizer) {
        print("Did capture some double-tap")
        
        if let viewWhichWasTapped = doubleTapRecognizer.view as? UIScrollView {
            print("Identified double-tapped view as scroll-view")
            if viewWhichWasTapped.zoomScale > 1.0 {
                print("Zoomed in, trying to zoom out")
                UIView.transition(with: viewWhichWasTapped, duration: 0.3,
                options: UIView.AnimationOptions.transitionCrossDissolve,
                animations: {
                    viewWhichWasTapped.zoomScale = 1.0
                  },
                completion: nil)
            }
            else {
                let tapPoint = doubleTapRecognizer.location(in: viewWhichWasTapped)
                let newZoomScale = min(viewWhichWasTapped.maximumZoomScale, 5.0)
                let viewSize = viewWhichWasTapped.bounds.size
                let zoomSize = CGSize(width: viewSize.width / newZoomScale,
                                      height: viewSize.height / newZoomScale)
                let newContentOffset = CGPoint(x: (tapPoint.x - zoomSize.width * 0.5) * newZoomScale,
                                               y: (tapPoint.y - zoomSize.height * 0.5) * newZoomScale)
//                let zoomRect = CGRect(x: newContentOffset.x,
//                                      y: newContentOffset.y,
//                                      width: zoomSize.width,
//                                      height: zoomSize.height)
//
//                viewWhichWasTapped.zoom(to: zoomRect, animated: true)
                UIView.transition(with: viewWhichWasTapped, duration: 0.5,
                options: UIView.AnimationOptions.transitionCrossDissolve,
                animations: {
                    viewWhichWasTapped.zoomScale = newZoomScale
                    viewWhichWasTapped.contentOffset = newContentOffset
                    
                  },
                completion: nil)
                
//                print("Zoomed out, trying to zoom in")
//                let newZoomScale = viewWhichWasTapped.maximumZoomScale
//                let viewSize = viewWhichWasTapped.bounds.size
//                let zoomSize = CGSize(width: viewSize.width / newZoomScale,
//                                      height: viewSize.height / newZoomScale)
//                let zoomCenter = doubleTapRecognizer.location(in: viewWhichWasTapped)
//                print("Tap coordinates: ", zoomCenter)
//                let zoomFrame = CGRect(x: zoomCenter.x - zoomSize.width * 0.5,
//                                       y: zoomCenter.y - zoomSize.height * 0.5,
//                                       width: zoomSize.width,
//                                       height: zoomSize.height)
//                //Convert rect from scroll view coordinates to content view
//                //coordinates
//                let zoomFrameForContent = viewWhichWasTapped.contentView
//                viewWhichWasTapped.zoom(to: zoomFrame, animated: true)
            }
        }
        else {
            print("Unable to identify view which was double-tapped")
        }
    }
    
    //Mark -- UIScrollViewDelegate methods
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        //print("Scrollview is requesting view for zooming")
        //We try to get the CollectionViewCell contentView with tag.
        if let imageView = scrollView.viewWithTag(VIEWTAGPREFIX + 155) {
            return imageView
        }
        else {
            print("No image-View found within Scroll-View")
            return nil
        }
    }
    
    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        //Stub implementation
    }
    
    //Adjust image cell size on rotation
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        //super.traitCollectionDidChange(previousTraitCollection)
        
        let galleryView = self.resultsGalleryView!
        let lastVisibleIndexPath = galleryView.indexPathsForVisibleItems.last

        let searchFlowLayout = galleryView.collectionViewLayout as! UICollectionViewFlowLayout
        //Change item size for new layout
        searchFlowLayout.itemSize = CGSize(width: self.view.bounds.size.width,
                                          height: self.view.bounds.size.height)
        self.view.layoutIfNeeded()

        //Preserve collection-view scroll-state, if it existed
        if lastVisibleIndexPath != nil {
            galleryView.scrollToItem(at: lastVisibleIndexPath!, at: .centeredHorizontally, animated: false)
        }
    }
    
    deinit {
        print("Gallery Controller deinited successfully.")
    }
}
