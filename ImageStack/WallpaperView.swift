//
//  WallpaperViewController.swift
//  ImageStack
//
//  Created by Kierkegaard on 29/08/19.
//  Copyright © 2019 Moi. All rights reserved.
//

import Foundation
import UIKit

//Parameters of animation, such as CGRect min and max sizes,
//and animation timing CAN be exposed via API, but hasn't
//been done yet
class AnimatingRectImageView: UIImageView {
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame) 
        
        self.backgroundColor = UIColor.clear
        self.contentMode = UIView.ContentMode.scaleAspectFill
        self.translatesAutoresizingMaskIntoConstraints = false
        
        //Setup wallpaper-view animations
//        CATransaction.begin()
//        //CATransaction.setAnimationDuration(1.6)
//        CATransaction.setAnimationDuration(10.0)
//        CATransaction.setAnimationTimingFunction(CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear))
        
        var animationChain: [CABasicAnimation] = []
        func randomRect() -> CGRect {
            let rectHeight = CGFloat.random(in: 0.6...0.9)
            let rectWidth = CGFloat.random(in: 0.6...0.9)
            let rectY = CGFloat.random(in: 0.0...(1-rectHeight))
            let rectX = CGFloat.random(in: 0.0...(1-rectWidth))
            
            return CGRect(x: rectX, y: rectY, width: rectWidth, height: rectHeight)
        }
        
        //How many frame changes do you want per cycle
        let maxAnimIndex = 3
        let animDuration = 10.0
        var prevRect: CGRect?
        for i in 0...maxAnimIndex {
            if prevRect == nil {
                prevRect = randomRect()
            }
                
            let rectAnimation = CABasicAnimation(keyPath: #keyPath(CALayer.contentsRect))
            rectAnimation.fromValue = prevRect
            //Store next prevRect
            prevRect = randomRect()
            rectAnimation.toValue = prevRect
            rectAnimation.fillMode = CAMediaTimingFillMode.forwards
            rectAnimation.isRemovedOnCompletion = false
            
            rectAnimation.duration = animDuration
            rectAnimation.beginTime = Double(i) * animDuration
//            rectAnimation.autoreverses = true
//            rectAnimation.repeatCount = Float.infinity
            animationChain.append(rectAnimation)
        }
        
        let animationGroup = CAAnimationGroup()
        animationGroup.animations = animationChain
        animationGroup.duration = Double(maxAnimIndex+1) * animDuration
        animationGroup.autoreverses = true
        animationGroup.repeatCount = Float.infinity
        self.layer.add(animationGroup, forKey: nil)
//        self.layer.add(wpAnimation, forKey: #keyPath(CALayer.contentsRect))
//        CATransaction.commit()
    }

}
