//
//  SearchConcierge.swift
//  ImageStack
//
//  Created by Kierkegaard on 28/08/19.
//  Copyright © 2019 Moi. All rights reserved.
//

import Foundation
import UIKit
import Dispatch

class ImageCache {
    
    enum SizeClass {
        case Small, Large
    }
    
    static let shared = ImageCache()
    private init() {
        //Creates a SERIAL queue with default priority
        cacheQueue = DispatchQueue.init(label: "com.ideveloper.imagestack.privatecachequeue")
        //Using an operation queue with the cache Dispatch queue underlying it,
        //as the core operation queue for URLSession, will hopefully channel
        //all delegate / closure callbacks occur on the corresponding
        //operation - /or - Dispatch Queue. We'll have to test to see if that
        //is the case.
        cacheOperationQueue = OperationQueue()
        cacheOperationQueue.underlyingQueue = cacheQueue
        let sessionConfig = URLSessionConfiguration.ephemeral
        //We CAN use responsiveData because most of the time we are only loading thumbnails
        //so want a quick response for that. Full-size images are only downloaded once a while.
        sessionConfig.networkServiceType = NSURLRequest.NetworkServiceType.responsiveData //or .default
        imageDLSession = URLSession.init(  configuration: sessionConfig,
                                           delegate: nil,
                                           //This is to ensure that completion handlers are called-back
                                           //on THIS operation queue itself, and prevents synchronization
                                           //issues or more complex handling
                                           delegateQueue: cacheOperationQueue)
        clearCacheFolder()
    }
    
    private let cacheQueue: DispatchQueue
    private let cacheOperationQueue: OperationQueue
    private let imageDLSession: URLSession
    private let maxImagesPerSizeClass: [SizeClass: Int] = [.Small: 50, .Large: 10]
    private var imageCachePerSizeClass: [SizeClass: [URL: UIImage]] = [.Small: [:], .Large: [:]]
    private var filePathsForURLs: [URL: URL] = [:]
    private var urlsWithPendingResponse: Set<URL> = []
    
    func getImage(forURL url: URL, sizeClass: SizeClass, onSuccess: @escaping (UIImage)->(), onFailure: @escaping (Error)->()) {
//        if !urlsWithPendingResponse.contains(url) {
//            print("Image request cancelled for url. Dropping request", url)
//            return
//        }
//        else
        self.cacheQueue.async {
            
            if let image = self.imageCachePerSizeClass[sizeClass]![url] {
                print("Found image in memory for url " + String(describing: url))
                DispatchQueue.main.async { onSuccess(image) }
                
                return
            }
            else if let cachedFileUrl = self.filePathsForURLs[url] {
                do {
                    print("Url found in file-cache. Attempting to read")
                    let fileData = try Data(contentsOf: cachedFileUrl)
                    print("Data read. Attempting to load into image")
                    
                    if let image = UIImage(data: fileData) {
                        
                        print("Image loaded. Waiting for return to caller")
                        if self.imageCachePerSizeClass[sizeClass]?.count ==
                            self.maxImagesPerSizeClass[sizeClass] {
                            let keyForOldestImageInCache = self.imageCachePerSizeClass[sizeClass]!.first!.key
                            self.imageCachePerSizeClass[sizeClass]?.removeValue(forKey: keyForOldestImageInCache)
                        }
                        self.imageCachePerSizeClass[sizeClass]![url] = image
                        
                        DispatchQueue.main.async { onSuccess(image) }
                        return
                    }
                    else {
                        print("Failed to convert data loaded from file into image", cachedFileUrl)
                    }
                }
                catch {
                    print("Unable to read image data from file", cachedFileUrl)
                    print("Because of error", error)
                }
            }
            else {
                print("URL " + String(describing:url) + " not found in-memory cache or file cache")
            }
            
            //
            //
            //
            //  DISCARD ALL SEMPAHORES. USE DISPATCH-QUEUE. OTHERWISE AT EACH LINE THE STATE
            //  CAN CHANGE. We think about operation sequence in blocks, with points of
            //  asynchrony being entry and exit of functions. So the best way to map
            //  this thinking to multi-threaded, is to have each logically contiguous
            //  and consistent block run on a common background thread (specific to each
            //  class or logical flow, say)
            //
            
            
            
            print("Attempts to use image cache or image cache urls failed. Proceeding to download")
            self.urlsWithPendingResponse.insert(url)
            
            let imageDownloadTask = self.imageDLSession.downloadTask(with: url, completionHandler: {
                (fileCacheUrl: URL?, response: URLResponse?, error: Error?) -> () in
                
                //Deliberate cacheQueue scheduling is not required, since completion callback
                //WILL occur on cacheQueue itself automatically, because of use of
                //imageDLSession, which has been configured to use cacheOperationQueue,
                //which has been configured to use cacheQueue as its underlying session.
                //So callbacks MUST hopefully occur on cacheQueue
                //self.cacheQueue.async {
                if !self.urlsWithPendingResponse.contains(url) {
                    print("Image request cancelled for url. Dropping request", url)
                    return
                }
                else {
                    //Now that the response has been received.
                    //Remove from pending response. This whole flow
                    //has to be revisited once file reads/writes are
                    //also background-threaded
                    self.urlsWithPendingResponse.remove(url)
                }
                    
                if error != nil {
                    print("Debug: Error response")
                    DispatchQueue.main.async { onFailure(error!) }
                    return
                }
                else if let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode != 200 {
                    print("Image download for URL resulted in status error:", httpResponse.statusCode, url)
                    DispatchQueue.main.async { onFailure(error!) }
                    return
                }
                else if let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200, fileCacheUrl != nil {
                    print("Debug: Image downloaded. Moving to cache")
                    
                    //Since filePathsforURLs is being accessed below, and since
                    //file-MOVING is an inexpensive operation, do it on the
                    //main thread itself. If it were file-copying, background would be
                    //mandatory
                    do {
                        let filename = fileCacheUrl!.lastPathComponent
                        print("Debug: Locating cache folder")
                        let systemCachesFolder = try FileManager.default.url(for: .cachesDirectory,
                                                                             in: .userDomainMask,
                                                                             appropriateFor: nil,
                                                                             create: true)
                        let cacheFolderUrl = systemCachesFolder.appendingPathComponent("VolatileImageCaches")
                        try FileManager.default.createDirectory(at: cacheFolderUrl, withIntermediateDirectories: true)
                        let newFileurl = cacheFolderUrl.appendingPathComponent(filename)
                        print("Debug: Expected file url: " , newFileurl)
                    
                        try FileManager.default.moveItem(at: fileCacheUrl!, to: newFileurl)
                        self.filePathsForURLs[url] = newFileurl
                        print("Debug: File move finished. Expecting image retrieval on main thread")
                        
                        
                        //Now load image from fileUrl and hand it back to caller
                        self.getImage(forURL: url, sizeClass: sizeClass, onSuccess: onSuccess, onFailure: onFailure)
                        return
                    }
                    catch {
                        let fileError = GeneralError.withMessage("Unable to move temporary download file to cache, with error" + String(describing: error))
                        DispatchQueue.main.async { onFailure(fileError) }
                        return
                    }
                }
                else {
                    let unknownError = GeneralError.withMessage("Image download for url " + url.absoluteString + " failed for unknown reason. HTTP Response was " + String(describing: response))
                    DispatchQueue.main.async { onFailure(unknownError) }
                    return
                }
            })
            imageDownloadTask.resume()
        }
    }
    
    func cancelGetImage(forURL url:URL) {
        self.cacheQueue.async {
            print("Cancelling image request for url.", url)
            self.urlsWithPendingResponse.remove(url)
        }
    }
    
    func clearCache() {
        self.cacheQueue.async {
            //First reset internal dictionaries
            self.imageCachePerSizeClass = [.Small: [:], .Large: [:]]
            self.filePathsForURLs = [:]
            self.urlsWithPendingResponse = []
            
            self.clearCacheFolder()
        }
    }
    
    func clearCacheFolder() {
        do {
            print("Debug: Locating cache folder")
            let systemCachesFolder = try FileManager.default.url(for: .cachesDirectory,
                                                                 in: .userDomainMask,
                                                                 appropriateFor: nil,
                                                                 create: false)
            let cacheFolderUrl = systemCachesFolder.appendingPathComponent("VolatileImageCaches")
        
            print("Attempting to delete cache-folder at path" , cacheFolderUrl)
        
            try FileManager.default.removeItem(at: cacheFolderUrl)
            print("Did clear cache successfully")
        }
        catch {
            print("Error in deleting cache folder:", error)
        }
    }
    
}
