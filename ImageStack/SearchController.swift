//
//  SearchController.swift
//  ImageStack
//
//  Created by Kierkegaard on 20/08/19.
//  Copyright © 2019 Moi. All rights reserved.
//

import UIKit

protocol ResultsPresenter {
    var delegate: ResultsDelegate? {get set}
    func reloadResults()
    func insertResults(inRange: CountableClosedRange<Int>)
    
    func focusOnResult(atIndex: Int, animated: Bool)
}

protocol ResultsDelegate {
    var resultCount: Int {get}
    func resultItem(atIndex: Int) -> BingImageResultItem
    func requestNextPageOfResults(fromIndex: Int)
    
    func presenter(_ presenter:UIViewController & ResultsPresenter, didFocusOnResultAtIndex index: Int)
    func presenterDidRequestToDismiss(_ presenter:UIViewController & ResultsPresenter, animated: Bool)
}

extension ResultsPresenter {
    func focusOnResult(atIndex index: Int, animated: Bool) {
        //Default implementation, makes this method optional
        print("Warning: Called default implementation for focusOnResult in ResultsPresenter, with index", index)
    }
}

extension ResultsDelegate {
    func presenter(_ presenter:UIViewController & ResultsPresenter, didFocusOnResultAtIndex index: Int) {
        //Default implementation, makes this method optional
        print("Warning! Called default implementation of didFocusOnResult from presenter", presenter)
    }
    
    func presenterDidRequestToDismiss(_ presenter:UIViewController & ResultsPresenter, animated: Bool) {
        //Default implementation, makes this method optional
        print("Warning! Called default implementation of didRequestToDismiss from presenter", presenter)
    }
}

class SearchController: UIViewController, UITextFieldDelegate, ResultsDelegate {

    enum EditIntent {
        case none, commit
    }
    
    private var searchField: UITextField?
    private var cancelEditUnderlay: UIControl?
    private var lastEditIntent = EditIntent.none
    private var searchFieldYCenterConstraint: NSLayoutConstraint?
    private var topBar: UIToolbar?
    //Only one of the two-below will be used
    private var loadingView: AnimatingGradientView?
    private var wallpaperController: WallpaperSearchController?
    
    
    private var activeQuery: String?
    private var nextOffsetForQuery: Int = 0
    private var searchResults: [BingImageResultItem] = []
    
    private var resultsGridPresenter: (UIViewController & ResultsPresenter)?
    //Keep gallery presenter as weak, in case it dismisses itself
    private weak var resultsGalleryPresenter: (UIViewController & ResultsPresenter)?
    
    
    
    override func loadView() {
        super.loadView()
        self.view.backgroundColor = UIColor.black
        
        //Loading animation view
        let loadingView = AnimatingGradientView.init()
        self.loadingView = loadingView
        loadingView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(loadingView)
        
        let loadingLeft = loadingView.leadingAnchor.constraint(equalTo:self.view.leadingAnchor)
        let loadingRight = loadingView.trailingAnchor.constraint(equalTo:self.view.trailingAnchor)
        let loadingTop = loadingView.topAnchor.constraint(equalTo:self.view.topAnchor)
        let loadingBottom = loadingView.bottomAnchor.constraint(equalTo:self.view.bottomAnchor)
        NSLayoutConstraint.activate([loadingLeft, loadingRight, loadingTop, loadingBottom])
        
        //Setup wallpaper view
//        let wallpaperController = WallpaperSearchController.init()
//        self.wallpaperController = wallpaperController
//        let wpView = wallpaperController.view!
//        //wpView.isHidden = true
//        self.view.addSubview(wpView)
//
//        wpView.translatesAutoresizingMaskIntoConstraints = false
//        let wpLeft = wpView.leadingAnchor.constraint(equalTo:self.view.leadingAnchor)
//        let wpRight = wpView.trailingAnchor.constraint(equalTo:self.view.trailingAnchor)
//        let wpTop = wpView.topAnchor.constraint(equalTo:self.view.topAnchor)
//        let wpBottom = wpView.bottomAnchor.constraint(equalTo:self.view.bottomAnchor)
//        NSLayoutConstraint.activate([wpLeft, wpRight, wpTop, wpBottom])
        
        
        //Setup search-field and keyboard delegate
        let searchField = UITextField.init(frame:CGRect.zero)
        self.searchField = searchField
        searchField.borderStyle = UITextField.BorderStyle.roundedRect
        searchField.translatesAutoresizingMaskIntoConstraints = false
        searchField.backgroundColor = UIColor.white
        searchField.placeholder = "Search images on Bing"
        searchField.font = UIFont.systemFont(ofSize: 24)
        searchField.adjustsFontSizeToFitWidth = false
        searchField.clearsOnBeginEditing = false
        searchField.clearButtonMode = UITextField.ViewMode.whileEditing
        searchField.returnKeyType = UIReturnKeyType.search
        searchField.autocorrectionType = UITextAutocorrectionType.yes
        //Added accessory view in the hope that it would fix keyboard notifications
        //searchField.inputAccessoryView = UIView.init(frame:CGRect.zero)
        //searchField.spellCheckingType = UITextSpellCheckingType.no
        searchField.delegate = self
        self.view.addSubview(searchField)
        
        
        let fieldCLeft = searchField.leadingAnchor.constraint(equalTo:self.view.leadingAnchor, constant: 40)
        let fieldCRight = searchField.trailingAnchor.constraint(equalTo:self.view.trailingAnchor, constant: -40)
        let fieldCHeight = searchField.heightAnchor.constraint(greaterThanOrEqualToConstant: 50)
        let fieldYCenter = searchField.centerYAnchor.constraint(equalTo:self.view.centerYAnchor)
        self.searchFieldYCenterConstraint = fieldYCenter
        NSLayoutConstraint.activate([fieldCLeft, fieldCRight, fieldCHeight, fieldYCenter])
        
        //Subscribe for keyboard appearance/disappearance notifications
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChangeFrame(notification:)), name: UITextField.keyboardWillChangeFrameNotification, object: nil)
        
        //Translucent Black toolbar behind text-field
        let topBar = UIToolbar.init()
        self.topBar = topBar
        topBar.alpha = 0.0
        topBar.barStyle = UIBarStyle.blackTranslucent
        //topBar.barPosition = UIBarPosition.UIBarPositionTopAtt
        self.view.insertSubview(topBar, belowSubview: searchField)
        
        topBar.translatesAutoresizingMaskIntoConstraints = false
        let tpLeft = topBar.leadingAnchor.constraint(equalTo:self.view.leadingAnchor)
        let tpRight = topBar.trailingAnchor.constraint(equalTo:self.view.trailingAnchor)
        let tpTop = topBar.topAnchor.constraint(equalTo:self.view.topAnchor)
        let tpHeight = topBar.heightAnchor.constraint(equalToConstant: 110.0)
        NSLayoutConstraint.activate([tpLeft, tpRight, tpTop, tpHeight])
        
        let cancelEditUnderlay = UIControl.init()
        self.cancelEditUnderlay = cancelEditUnderlay
        cancelEditUnderlay.backgroundColor = UIColor.black.withAlphaComponent(0.2)
        cancelEditUnderlay.addTarget(self, action: #selector(cancelEditing(sender:forEvent:)), for: UIControl.Event.touchUpInside)
        self.view.insertSubview(cancelEditUnderlay, belowSubview: topBar)
        cancelEditUnderlay.alpha = 0.0
        cancelEditUnderlay.isUserInteractionEnabled = false
        
        cancelEditUnderlay.translatesAutoresizingMaskIntoConstraints = false
        let cancelLeft = cancelEditUnderlay.leadingAnchor.constraint(equalTo:self.view.leadingAnchor)
        let cancelRight = cancelEditUnderlay.trailingAnchor.constraint(equalTo:self.view.trailingAnchor)
        let cancelTop = cancelEditUnderlay.topAnchor.constraint(equalTo:self.view.topAnchor)
        let cancelBottom = cancelEditUnderlay.bottomAnchor.constraint(equalTo:self.view.bottomAnchor)
        NSLayoutConstraint.activate([cancelLeft, cancelRight, cancelTop, cancelBottom])
        
        
        
        //Setup image-search results presenters
        self.resultsGridPresenter = ResultsGridController.init()
        self.resultsGridPresenter!.delegate = self
        let resultsGridView = self.resultsGridPresenter!.view!
        resultsGridView.backgroundColor = UIColor.clear
        resultsGridView.clipsToBounds = false
        self.view.insertSubview(resultsGridView, belowSubview:cancelEditUnderlay)
        
        resultsGridView.translatesAutoresizingMaskIntoConstraints = false
        let rgLeft = resultsGridView.leadingAnchor.constraint(equalTo:self.view.leadingAnchor)
        let rgRight = resultsGridView.trailingAnchor.constraint(equalTo:self.view.trailingAnchor)
        let rgTop = resultsGridView.topAnchor.constraint(equalTo:topBar.bottomAnchor)
        let rgBottom = resultsGridView.bottomAnchor.constraint(equalTo:self.view.bottomAnchor)
        NSLayoutConstraint.activate([rgLeft, rgRight, rgTop, rgBottom])
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //self.navigationItem.title = "Splash"
        //self.loadingView?.startAnimating()
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.0) {
            self.loadingView?.startAnimating()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2) {
            self.wallpaperController?.startLoadingWallpapers()
        }
    }
    
//    override func viewDidLayoutSubviews() {
//        print("Search field frame: ", self.searchField?.frame)
//    }
    
    @objc func keyboardWillChangeFrame(notification: Notification) {
        print("Did get notification: ", notification.name)
        
        //Animate text-field to ANY new position, only if Text-field is empty
        //or WILL be empty, based on edit intent
        var expectedFinalText = self.searchField?.text
        //If editIntent is not "commit", search field text will
        //be reset to "self.activeQuery". So change expectedFinalText
        //to the same
        if self.lastEditIntent == .none {
            expectedFinalText = self.activeQuery
        }
        
        
        if expectedFinalText?.count ?? 0 == 0 {
            if let oldFieldCenterConstraint = self.searchFieldYCenterConstraint {

                let userInfo = notification.userInfo!
//                //let kbBeginFrame = userInfo[UITextField.keyboardFrameBeginUserInfoKey]
                let kbEndFrame = (userInfo[UITextField.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
                let kbEndHeight = self.view.bounds.height - self.view.convert(kbEndFrame, from: nil).minY
                let kbAnimationDuration = (userInfo[UITextField.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0.25
                let kbAnimationConst = ((userInfo[UITextField.keyboardAnimationCurveUserInfoKey] as? NSNumber)?.uintValue ?? (UIView.AnimationOptions.curveEaseInOut.rawValue >> 16)) << 16
                let kbAnimationOption = UIView.AnimationOptions.init(rawValue: UInt(kbAnimationConst))

//                let newFieldCenterConstraint = self.searchField!.centerYAnchor.constraint(equalTo:self.view.centerYAnchor, constant: -kbEndHeight / 2.0)
                var newFieldCenterConstraint: NSLayoutConstraint? = nil
                //If keyboard is being removed, restore textfield to CENTER of view
                //When keyboard is removed, as per above commented formula,
                //kbEndHeight is expected to be zero. But having a slightly
                //larger value to account for calculation or rounding errors
                if kbEndHeight < 5.0 {
                    newFieldCenterConstraint = self.searchField!.centerYAnchor.constraint(equalTo:self.view.centerYAnchor, constant: 0.0)
                }
                //If keyboard is being ADDED, then push textfield to very top
                else {
                    newFieldCenterConstraint = self.searchField!.topAnchor.constraint(equalTo:self.view.topAnchor, constant: 40.0)
                }
                self.searchFieldYCenterConstraint = newFieldCenterConstraint!

                self.view.layoutIfNeeded()
                UIView.animate(withDuration: kbAnimationDuration, delay: 0.0, options: kbAnimationOption, animations: { () -> Void in
                        //Change text-field frame
                        oldFieldCenterConstraint.isActive = false
                        newFieldCenterConstraint!.isActive = true
                        self.view.layoutIfNeeded()
                    
                        //Make top-bar disappear whenever text-field goes back to center of screen
                        if kbEndHeight < 5.0 {
                            self.topBar!.alpha = 0.0
                        }
                        //Make top-bar appear whenever text-field goes back to TOP of screen
                        else {
                            self.topBar!.alpha = 1.0
                        }
                    }, completion: nil)
            }
        }
    }
    
    @objc func cancelEditing(sender: UIControl, forEvent event: UIControl.Event) {
//        //Debug
//        print("Did receive cancelEditing from sender and event respectively", sender, event)
//        if sender == self.cancelEditUnderlay {
//            print("Correct sender")
//        }
//        else {
//            print("Incorrect sender")
//        }
//        //Incorrect event is CALLED!! This maybe an XCode bug.
//        //Validate only sender as of now
//        if event == UIControl.Event.touchUpInside {
//            print("Correct event")
//        }
//        else {
//            print("Incorrect event")
//        }
            
        if sender == self.cancelEditUnderlay {
            self.lastEditIntent = .none
            self.searchField!.resignFirstResponder()
        }
    }
    
    //Mark -- UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.lastEditIntent = .commit
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("Search field did begin editing")
        self.cancelEditUnderlay!.isUserInteractionEnabled = true
        UIView.animate(withDuration: 0.25, animations: {
            self.cancelEditUnderlay!.alpha = 1.0
        })
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) -> Void {
        print("Search field did finish editing")
        self.cancelEditUnderlay!.isUserInteractionEnabled = false
        UIView.animate(withDuration: 0.25, animations: {
            self.cancelEditUnderlay!.alpha = 0.0
        })
        
        if self.lastEditIntent == .commit {
            //Reset edit-intent immediately after using it
            self.lastEditIntent = .none
            let newQuery = textField.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            if newQuery != self.activeQuery {
                //Cancel previous query
                BingRequestor.shared.cancelAllSearches()
                //Start a whole new query.
                self.nextOffsetForQuery = 0
                self.searchResults = []
                self.activeQuery = newQuery
                ImageCache.shared.clearCache()
                self.resultsGridPresenter?.reloadResults()
                self.resultsGalleryPresenter?.reloadResults()
                
                self.requestNextPageOfResults(fromIndex: 0)
            }
        }
        else {
            //Reset text on searchField
            self.searchField!.text = self.activeQuery
        }
    }
    
    //Mark -- Search and Image Retrieval logic
    func requestNextPageOfResults(fromIndex: Int) {
        if self.activeQuery != nil && self.activeQuery?.count ?? 0 > 0 && fromIndex >= self.searchResults.count {
            BingRequestor.shared.searchImages(withQuery: self.activeQuery!, completion: { result in
                let result: Status = result()
                switch result {
                    case let .success(summary, debugMsgs, searchResults, nextOffset):
    //                    //Simple test for API response
    //                    let message = "Retrieved \(searchResults.count) search results"
    //                    let alert = UIAlertController(title: "Success", message: message, preferredStyle: UIAlertController.Style.alert)
    //                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
    //                    self.present(alert, animated: true, completion: nil)
                        //Simple debug logs
                        print(summary)
                        print("Printing select data from first search result")
                        print("Thumb-url for first result: ", searchResults[0].thumbnailUrl)
    //                    //Advanced debug logs
    //                    for (type, message) in debugMsgs {
    //                        print(type, ": ", message)
    //                    }
                        
                        //Store search-results to internal model
                        //Store nextOffset from API response
                        self.searchResults += searchResults
                        self.nextOffsetForQuery = nextOffset>0 ? nextOffset : self.searchResults.count
                        
                        
                        //Provoke results presenters to insert new results
                        //in their presentation
                        let addedResultCount = searchResults.count
                        let totalResultCount = self.searchResults.count
                        let initialResultCount = totalResultCount - addedResultCount
                        
                        //Instead of reloading the whole results gallery,
                        //we only insert cells for the new data. This
                        //is accomplished by calling insertResults
                        //on all active presenters
                        let rangeToInsert: CountableClosedRange = initialResultCount...(totalResultCount - 1)
                        //print("Check if we are on main thread: ", Thread.isMainThread)
                        self.resultsGridPresenter?.insertResults(inRange: rangeToInsert)
                        self.resultsGalleryPresenter?.insertResults(inRange: rangeToInsert)
                        
                    
                    case let .failure(summary, debugMsgs, _):
    //                    let alert = UIAlertController(title: "Failure", message: summary, preferredStyle: UIAlertController.Style.alert)
    //                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
    //                    self.present(alert, animated: true, completion: nil)
                        print("Unable to load image search results. Retrying in 60 seconds")
                        print(summary)
                        print(debugMsgs)
    //                case .failure(let summary, let debugMsgs, .none):
    //                    let alert = UIAlertController(title: "Failure", message: summary, preferredStyle: UIAlertController.Style.alert)
    //                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
    //                    rootViewController.present(alert, animated: true, completion: nil)
    //                    print(summary)
    //                    print(debugMsgs)
                }
            },

          count: 30,
          nextOffset: self.nextOffsetForQuery)
        }
    }
    
    //Mark -- ResultsDelegate protocol methods
    var resultCount: Int {
        get {
            let resultsCount = self.searchResults.count
            print("Returning search result-count: " + String(resultsCount))
            return resultsCount
        }
    }
    
    func resultItem(atIndex index: Int) -> BingImageResultItem {
        assert(index >= 0 && index < self.searchResults.count, "Invalid results-index! Requested \(index) whereas expected in range 0...\(self.searchResults.count-1)")
        return self.searchResults[index]
    }
    
    func presenter(_ presenter:UIViewController & ResultsPresenter, didFocusOnResultAtIndex index: Int) {
        //If an item in grid presenter is selected, show
        //the gallery presenter
        print("Checking which presenter has acquired user focus")
        if let gridPresenter = self.resultsGridPresenter, presenter == gridPresenter {
            print("Responding to focus in Grid presenter")
            assert(self.resultsGalleryPresenter == nil, "The old gallery presenter has still not been unwinded")
            let galleryPresenter = ResultsGalleryController.init()
            self.resultsGalleryPresenter = galleryPresenter
            galleryPresenter.delegate = self
            galleryPresenter.focusOnResult(atIndex: index, animated: false)
            //Disable modal-presentation style
            galleryPresenter.modalPresentationStyle = .overFullScreen
            self.present(galleryPresenter, animated: true, completion: nil)
        }
        //Use callbacks from gallery presenter to keep
        //the grid presenter in sync
        else if let galleryPresenter = self.resultsGalleryPresenter, presenter == galleryPresenter {
            print("Responding to focus in Gallery presenter")
            self.resultsGridPresenter?.focusOnResult(atIndex: index, animated: false)
        }
    }
    
    func presenterDidRequestToDismiss(_ presenter:UIViewController & ResultsPresenter, animated: Bool = true) {
        print("Responding to presenter request to dismiss")
        if let galleryPresenter = self.resultsGalleryPresenter, presenter == galleryPresenter {
            galleryPresenter.dismiss(animated: animated, completion: nil)
            //Unwind old gallery presenter
            self.resultsGalleryPresenter = nil
        }
    }
}
