//
//  BingRequester.swift
//  ImageStack
//
//  Created by Kierkegaard on 25/08/19.
//  Copyright © 2019 Moi. All rights reserved.
//

import Foundation
import UIKit
//enum Status {case success, failure}
//
//struct Result {
//    var status: Status
//    var summaryText: String
//    var detailedText: String
//    var payload: Any?
//}

enum Status {
    case success(summary: String, debugMsgs: [String: String], resultsList: [BingImageResultItem], nextOffset: Int)
    case failure(summary: String, debugMsgs: [String: String], error: Error?)
}

class BingImageResultItem {
    let imageID: String
    let thumbnailUrl: URL
    let fullImageUrl: URL
    let thumbnailSize: CGSize
    let imageSize: CGSize
    let accentColor: UIColor
    
    init(withJSONResultItem resultItem:[String:Any]) throws {
        if  let thumbUrlString = resultItem["thumbnailUrl"] as? String,
            let fullImageUrlString = resultItem["contentUrl"] as? String,
            let thumbSize = resultItem["thumbnail"] as? [String:Any],
            let thumbWidth = thumbSize["width"] as? Int,
            let thumbHeight = thumbSize["height"] as? Int,
            let imageWidth = resultItem["width"] as? Int,
            let imageHeight = resultItem["height"] as? Int,
            var accentColorHexString = resultItem["accentColor"] as? String,
            let imageIDString = resultItem["imageId"] as? String
            {
                thumbnailUrl = URL(string: thumbUrlString)!
                fullImageUrl = URL(string: fullImageUrlString)!
                thumbnailSize = CGSize(width: thumbWidth, height: thumbHeight)
                imageSize = CGSize(width: imageWidth, height: imageHeight)
                imageID = imageIDString
                
                //Get accent color
                if accentColorHexString.hasPrefix("#") {
                    accentColorHexString.remove(at: accentColorHexString.startIndex)
                }
                if accentColorHexString.count != 6 {
                    accentColor = UIColor.gray
                }
                else {
                    var rgbValue:UInt64 = 0
                    Scanner(string: accentColorHexString).scanHexInt64(&rgbValue)
                    
                    accentColor = UIColor(
                        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
                        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
                        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
                        alpha: CGFloat(1.0)
                    )
                }
        }
        else {
            throw GeneralError.withMessage("Results formatted incorrectly. Unable to extract keys for ResultItem")
        }
    }
}

class BingRequestor {
    
    static let shared = BingRequestor.init()
    //let imageSearchHost = "api.cognitive.microsoft.com"
    //let imageSearchHost = "myimagesearchsubscription.cognitiveservices.azure.com"
    let imageSearchHost = "bingimagesearchresource.cognitiveservices.azure.com"
    let imageSearchPath = "/bing/v7.0/images/search"
    //let imageSearchPath = "/bing/v7.0"
    var additionalHeaders = ["Accept": "application/json",
                             "Ocp-Apim-Subscription-Key": REPLACE_WITH_VALID_SUBSCRIPTION_KEY__AS_STRING ]
    let ClientIDHeader = "X-MSEdge-ClientID"
    //Above will be mutated to add:
    //"X-MSEdge-ClientID" provided in response to first request
    struct SearchContext: Hashable {
        let query: String
        let offset: Int
    }
    var queriesWithPendingResponse: Set<SearchContext> = []
    
    var bingSession: URLSession?
    private init() {
        let sessionConfig = URLSessionConfiguration.ephemeral
        sessionConfig.networkServiceType = NSURLRequest.NetworkServiceType.responsiveData //or .default
        self.bingSession = URLSession.init(configuration: sessionConfig,
                                           delegate: nil,
                                           //This is to ensure that completion handlers (and ideally delegate
                                           //callbacks occur on main thread
                                           delegateQueue: OperationQueue.main)
    }
    
    func searchImages(withQuery query: String, completion: @escaping (() -> Status ) -> (), count: Int = 15, nextOffset: Int = 0) {
        let thisSearchContext = SearchContext(query: query, offset: nextOffset)
        
        objc_sync_enter(self)
            if self.queriesWithPendingResponse.contains(thisSearchContext) {
                print("Search already requested for context " + String(describing: thisSearchContext) + ". Dropping.")
                objc_sync_exit(self)
                return
            }
        
                
        self.queriesWithPendingResponse.insert(thisSearchContext)
        objc_sync_exit(self)
        
        print("Beginning search for context " + String(describing: thisSearchContext))
        //URL-Query composition using URLComponents and URLQueryItem.
        //Based on advice from stackoverflow:
        //https://stackoverflow.com/questions/24551816/swift-encode-url
        //and this blog:
        //https://cocoacasts.com/working-with-nsurlcomponents-in-swift
        var searchUrlComponents = URLComponents()
        searchUrlComponents.scheme = "https"
        searchUrlComponents.host = self.imageSearchHost
        searchUrlComponents.path = self.imageSearchPath
        let searchQueryItem = URLQueryItem(name: "q", value: query)
        let countItem = URLQueryItem(name: "count", value: String(count))
        let offsetItem = URLQueryItem(name: "offset", value: String(nextOffset))
        searchUrlComponents.queryItems = [searchQueryItem, countItem, offsetItem]
        
        print("Debug: URL Components URL:", searchUrlComponents.url ?? "Invalid Url")
        var searchRequest = URLRequest.init(url: searchUrlComponents.url!)
        for (header, value) in additionalHeaders {
            searchRequest.setValue(value, forHTTPHeaderField: header)
        }
        
//        //Also change below to responsiveData and see if there is a difference
//        searchRequest.networkServiceType = NSURLRequest.NetworkServiceType.default
        
        //Initiate request
        //Using configured session for callbacks on main-thread.
        //Otherwise, use URLSession.shared if you want to manage
        //main-thread callbacks manually, after all the key logic
        //is done.
        let searchQueryTask = self.bingSession!.dataTask(with: searchRequest, completionHandler: {(data: Data?, urlResponse: URLResponse?, error: Error?) -> Void in
            
            objc_sync_enter(self)
            if !self.queriesWithPendingResponse.contains(thisSearchContext) {
                //This search is cancelled. Return.
                objc_sync_exit(self)
                return
            }
            
            self.queriesWithPendingResponse.remove(thisSearchContext)
            objc_sync_exit(self)
            
            
            //MIGHT HAVE TO PERFORM BELOW  RESPONSE-PROCESSING ON MAIN-THREAD,
            //or manage individual callbacks and object-property accesses (such as on self)
            //on the appropriate threads
            if let response = urlResponse as? HTTPURLResponse {
                if let error = error {
                    let errorSummary = "Search query for context " + String(describing: thisSearchContext) + " failed with HTTP status code: " +
                        String(response.statusCode)
                    let debugMsgs = [
                        "error": error.localizedDescription,
                        "Response Headers": String(describing: response.allHeaderFields),
                    ]
                    let failureStatus = Status.failure(summary: errorSummary,
                                                       debugMsgs: debugMsgs,
                                                       error: error)
                    DispatchQueue.main.async() { completion({ return failureStatus }) }
                }
                else {
                    //Request was successful.
                    let successSummary = "Search query for context " + String(describing: thisSearchContext) + "succeeded with response: \n" + String(response.statusCode)
                    let respHeaders = response.allHeaderFields
                    
                    if let clientId = respHeaders[self.ClientIDHeader] as? String {
                        //Set properties with synchronize
                        objc_sync_enter(self)
                            self.additionalHeaders[self.ClientIDHeader] = clientId
                        objc_sync_exit(self)
                    }
                    
                    
                    var debugMsgs = [ "Response Headers": String(describing: respHeaders) ]
                    
                    if let data = data {
                        debugMsgs[ "Raw data forced into UTF-8 String" ] = String(decoding: data, as: UTF8.self)
                        
                        do {
                            let jsonData = try JSONSerialization.jsonObject(with: data, options: []) as! [String: Any]
                            //Perform basic validation:
                            if let resultsType = jsonData["_type"] as? String, let queryContext = jsonData["queryContext"] as? [String:Any] {
                                let originalQuery = queryContext["originalQuery"] as? String ?? ""
                                //If it is the proper results type, and matching response to the
                                //query for which this function has been called, proceed
                                //with processing, else fail silently
                                if resultsType == "Images" && originalQuery == query {
                                    if let resultsList = jsonData["value"] as? [[String:Any]] {
                                        var formattedResultsList: [BingImageResultItem] = []
                                        for (index, resultItem) in resultsList.enumerated() {
                                            do {
                                                let bingResultItem = try BingImageResultItem.init(withJSONResultItem: resultItem)
                                                    
                                                formattedResultsList.append(bingResultItem)
                                            }
                                            catch {
                                                //Silent failure
                                                print("Unable to parse result-item #\(index) correctly")
                                                print("Debug: ", resultItem)
                                            }
                                        }
                                        
                                        let resultsNextOffset = jsonData["nextOffset"] as? Int ?? 0
                                        //Below uncommented code was wrapped in if-let, but then it fails silently
                                        debugMsgs["JSON re-encoded as String"] = String(describing: jsonData)
                                        let successStatus = Status.success( summary: successSummary,
                                                                            debugMsgs: debugMsgs,
                                                                            resultsList: formattedResultsList,
                                                                            nextOffset: resultsNextOffset)
                                        
                                        DispatchQueue.main.async() { completion({ return successStatus }) }
                                    }
                                    else {
                                        throw GeneralError.withMessage("Results not packaged as per expectation. Inspect raw string to see if \"value\" key is missing")
                                    }
                                }
                                else {
                                    throw GeneralError.withMessage("Incorrect result-type: \(resultsType) or originalQuery: \(originalQuery)")
                                }
                            }
                            else {
                                throw GeneralError.withMessage("Search result not packaged correctly. Missing either _type or queryContext")
                            }
                            
//                            else {
//                                let errorSummary = "Parsed JSON empty"
//                                debugMsgs["Erroneous success status"] = successSummary
//                                let failureStatus = Status.failure( summary: errorSummary,
//                                                                    debugMsgs: debugMsgs,
//                                                                    error: nil)
//
//                                DispatchQueue.main.async() { completion({ return failureStatus }) }
//                            }
                        }
                        catch {
                            var errorDescription: String
                            if error is Error {
                                errorDescription = error.localizedDescription
                                if errorDescription == "" {
                                    errorDescription = "No error message"
                                }
                            }
                            else {
                                errorDescription = "Unknown error"
                            }
                            
                            
                            let errorSummary = "Error parsing JSON - " + errorDescription
                            debugMsgs["Erroneous success status"] = successSummary
                            let failureStatus = Status.failure( summary: errorSummary,
                                                                debugMsgs: debugMsgs,
                                                                error: error )
                            
                            DispatchQueue.main.async() { completion({ return failureStatus }) }
                        }
                    }
                    else {
                        let errorSummary = "Search query for \"" + query + "\" returned no data"
                        debugMsgs["Erroneous success status"] = successSummary
                        let failureStatus = Status.failure( summary: errorSummary,
                                                            debugMsgs: debugMsgs,
                                                            error: nil)
                        
                        DispatchQueue.main.async() { completion({ return failureStatus }) }
                    }
                }
            }
        })
        searchQueryTask.resume()
    }
    
    func cancelAllSearches() {
        //Reset queries with pending response,
        //so that no callbacks occur from delayed searches
        //BUT, also need to clean up the actual url requests
        //a little later.
        objc_sync_enter(self)
        self.queriesWithPendingResponse = []
        objc_sync_exit(self)
    }
}
